package com.yolo.cache.storage;

import android.text.TextUtils;

import java.security.InvalidParameterException;

public class YoloCacheConstants extends CacheConstants {

    /**
     * 文件存储功能Key前缀
     * Key配置规则：例："file_key_cloud_ad"
     * 1：第一部分："file"设置存储格式为文件类型，必须以"file_key_"开头
     * 2：第二部分："cloud"为文件名的唯一标识 ,"cloud"为文件存储的唯一标识
     */
    public static final String KEY_SPACE_TYPE_FILE = "file_key_";
    public static final String KEY_SPACE_FILE_CLOUD = "file_key_cloud_";


    /**
     * SharePreferences存储功能Key前缀
     * Key配置规则：例："sp_key_credit_invite"
     * 1：第一部分："sp"设置存储格式为SharePreferences类型，必须以"sp_key_"开头
     * 2：第二部分："credit"为SharePreferences的配置文件名的唯一标识，此命名需和功能模块负责人确认再命名
     * 3：第三部分：为缓存自定义命名，此命名由使用者自己命名，新建的key需在各自功能模块包名下统一一个类进行维护，不能写在这里
     */
    public static final String KEY_SPACE_SP_BASE = CacheConstants.KET_SPACE_SP_IMG_CACHE;
    public static final String KEY_SPACE_SP_COMMON = "sp_key_common_";
    public static final String KEY_SPACE_SP_CORE = "sp_key_core_";
    public static final String KET_SPACE_SP_INVITE = "sp_key_invite_";
    public static final String KET_SPACE_SP_INSTALL = "sp_key_install_";
    public static final String KET_SPACE_FILE_INSTALL = "file_key_install_";
    public static final String KEY_SPACE_SP_AUTH = "sp_key_auth_";
    public static final String KEY_SPACE_SP_APP = "sp_key_app_";
    public static final String KEY_SPACE_SP_CREDIT = "sp_key_credit_";

    public static final String SP_KEY_AUTH_USER = YoloCacheConstants.KEY_SPACE_SP_AUTH + "user";

    public static String optCacheKey(String primaryKey, String subKey) {
        if (TextUtils.isEmpty(primaryKey) || TextUtils.isEmpty(subKey)) {
            throw new InvalidParameterException();
        }
        return primaryKey + subKey;
    }

}
