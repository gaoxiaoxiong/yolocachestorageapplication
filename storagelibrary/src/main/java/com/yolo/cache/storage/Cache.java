package com.yolo.cache.storage;

/**
 * @author zhaibinme on 2018/7/21
 */
public interface Cache {

    <T> boolean put(String key, T value, boolean needEncrypt);

    <T> T get(String key, Class valueClass);

    <T> T get(String key, T defaultValue, Class valueClass);

    boolean delete(String key);

    boolean contains(String key);
}
