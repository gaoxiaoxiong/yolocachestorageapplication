package com.yolo.cache.storage;

import com.google.gson.GsonBuilder;

import android.content.Context;

import com.yolo.cache.storage.encrypt.ConcealEncryption;
import com.yolo.cache.storage.encrypt.Encryption;
import com.yolo.cache.storage.parser.GsonParser;
import com.yolo.cache.storage.parser.Parser;
import com.yolo.cache.storage.serializer.Serializer;
import com.yolo.cache.storage.serializer.YoloSerializer;

/**
 * @author zhaibinme on 2018/7/21
 */
public class CacheBuilder {

    private Context mContext;
    private Parser mParser;
    private Serializer mSerializer;
    private Encryption mEncryption;


    public CacheBuilder(Context context) {
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    public Parser getParser() {
        if (mParser == null) {
            mParser = new GsonParser(new GsonBuilder().serializeNulls().create());
        }
        return mParser;
    }

    public Serializer getSerializer() {
        if (mSerializer == null) {
            mSerializer = new YoloSerializer();
        }
        return mSerializer;
    }

    public Encryption getEncryption() {
        if (mEncryption == null) {
            mEncryption = new ConcealEncryption(mContext);
            if (!mEncryption.init()) {
                return null;
            }
        }
        return mEncryption;
    }
}
