package com.yolo.cache.storage;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yolo.cache.storage.storage.FileStorage;


/**
 * @author zhaibinme on 2018/7/21
 */
public class YoloCacheStorage {
    private static YoloCacheStorage mYoloCacheStorage = null;
    private static Cache mCache = null;

    public static YoloCacheStorage init(Context context) {
        /*try {
            CacheBuilder cacheBuilder = new CacheBuilder(context);
            mCache = new DefaultCache(cacheBuilder);
            FileStorage.init(context);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }*/

        if (mYoloCacheStorage == null){
            synchronized (YoloCacheStorage.class){
                if (mYoloCacheStorage == null){
                    mYoloCacheStorage = new YoloCacheStorage(context);
                }
            }
        }
        return mYoloCacheStorage;
    }

    private YoloCacheStorage(Context context){
        if (context == null){
            throw new IllegalStateException(" context is null");
        }

        try {
            if (mCache == null){
                CacheBuilder cacheBuilder = new CacheBuilder(context);
                mCache = new DefaultCache(cacheBuilder);
                FileStorage.init(context);
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

    }



    public static <T> boolean put(String key, @NonNull T value) {
        return put(key, value, false);
    }

    public static <T> boolean put(String key, @NonNull T value, boolean needEncrypt) {
        if (mCache == null) {
            return false;
        }
        return mCache.put(key, value, needEncrypt);
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        Boolean value = get(key, Boolean.class);
        if (value == null) {
            return defaultValue;
        }

        return value;
    }

    public static long getLong(String key, @Nullable long defaultValue) {
        Long value = (Long) get(key, Long.class);
        if (value == null) {
            return defaultValue;
        }
        return value;
    }

    public static int getInt(String key, @NonNull int defaultValue) {
        Integer value = (Integer) get(key, Integer.class);
        if (value == null) {
            return defaultValue;
        }
        return value;
    }

    public static String getString(String key, @NonNull String defaultValue) {
        String value = get(key, String.class);
        if (TextUtils.isEmpty(value) || "null".equals(value)) {
            return defaultValue;
        }
        return value;
    }

    public static Double getDouble(String key, @NonNull double defaultValue) {
        Double value = get(key, Double.class);
        if (value == null) {
            return defaultValue;
        }

        return value;
    }

    public static Float getFloat(String key, @NonNull float defaultValue) {
        Float value = get(key, Float.class);
        if (value == null) {
            return defaultValue;
        }

        return value;
    }

    private static <T> T get(String key, Class valueClass) {
        if (mCache == null) {
            return null;
        }
        T t = mCache.get(key, valueClass);
        return t;
    }

    /**
     * 支持对象，数组，set，map类型
     * @param key
     * @param defaultValue
     * @param <T>
     * @return
     */
    public static <T> T getData(String key, T defaultValue, Class valueClass) {
        if (mCache == null) {
            return defaultValue;
        }
        return mCache.get(key, defaultValue, valueClass);
    }

    public static boolean delete(String key) {
        if (mCache == null) {
            return false;
        }
        return mCache.delete(key);
    }

    public static boolean contains(String key) {
        if (mCache == null) {
            return false;
        }
        return mCache.contains(key);
    }

    public static  <T> String toEncrypt(T value) {
        if (!(mCache instanceof DefaultCache)) {
            return null;
        }

        return ((DefaultCache)mCache).toEncrypt( value);
    }

    public static <T> T getValueByDecrypt(String value, Class classValue) {
        if (!(mCache instanceof DefaultCache)) {
            return null;
        }

        return ((DefaultCache)mCache).getValueByDecrypt(value, classValue);
    }

}
