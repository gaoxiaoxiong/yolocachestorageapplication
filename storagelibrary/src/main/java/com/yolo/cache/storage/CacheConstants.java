package com.yolo.cache.storage;

/**
 * @author zhaibinme on 2018/7/21
 */
public class CacheConstants {

    // 无效的storage的key
    public static final String BASE_STORAGE_CONFIG = "base_storage_";

    public static final String COMMON_STORAGE_CONFIG = "base_storage_common";

    public static String parserKey(String value) {
        return BASE_STORAGE_CONFIG + value;
    }

    public static String KET_SPACE_SP_IMG_CACHE = "sp_key_common_img_cache_";

    public static final String KEY_SPACE_SP_HOME = "sp_key_home_";

    public static String KEY_REPORT_INSTALL_INFO_SERVER_SUCCESSFUL = KEY_SPACE_SP_HOME + "report_install_info_successful";
    // storage需要以sp_key或file_key开头
    public static String KEY_FIRST_OPEN_SUCCESS = KEY_SPACE_SP_HOME + "first_open_success";

    public static String KEY_HAS_REPORT_INSTALL_INFO = KEY_SPACE_SP_HOME + "has_report_install_info";

    public static final String KET_SPACE_FILE_INSTALL = "file_key_install_";
    public static final String KEY_USER_INSTALL_INFO = KET_SPACE_FILE_INSTALL + "info";
    public static final String KEY_USER_INSTALL_INFO_FOR_YOADX = KET_SPACE_FILE_INSTALL + "info_for_yoadx";
    public static final String KEY_USER_GAID = KET_SPACE_FILE_INSTALL + "_gaid";

    public static String KEY_FIRST_OPEN_DATE = KEY_SPACE_SP_HOME + "first_open_date";

    public static String KEY_DID_REPORT_24H_RETENTION = KEY_SPACE_SP_HOME + "did_report_24h_retention";

    public static String KEY_FIRST_OPEN_TS = KEY_SPACE_SP_HOME + "first_open_ts";

    public static final String KEY_FOR_APP_VERSION_CODE = KET_SPACE_SP_IMG_CACHE + "app_version_code";

    public static String KEY_OPEN_APP_COUNT = KEY_SPACE_SP_HOME + "open_app_count";

}
