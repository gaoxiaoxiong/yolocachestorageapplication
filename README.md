# yoloCacheStorage使用教程

#### 依赖引入
```
    implementation 'androidx.startup:startup-runtime:1.1.1'
    implementation 'com.google.code.gson:gson:2.9.0'
    implementation 'com.yolo:arch-log:1.0.4'
    implementation "com.facebook.conceal:conceal:1.1.3@aar"
    implementation 'com.yolo:arch-storage:1.0.4'
```

#### 初始化
这里初始化有2种方式，第一种就是在清单文件里面配置，第二种是在Application里面配置。**选择其中一种**配置即可
```
<application>
        <provider
            android:name="androidx.startup.InitializationProvider"
            android:authorities="${applicationId}.androidx-startup"
            android:exported="false"
            tools:node="merge" >
            <meta-data
                android:name="com.yolo.cache.storage.initializer.YoloStorageHttpInitializer"
                android:value="androidx.startup" />
        </provider>

    </application>
```
或者 Application里面配置
```
class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        //这是第二种配置方式，第一种是在清单文件里面配置
        //YoloCacheStorage.init(this)
    }
}
```

#### 使用
```
   YoloCacheStorage.put("sp_userName","张三") //存
   YoloCacheStorage.getString("sp_userName","") //取
   注意：key 必须以 sp 或 file 开头的名词
```
