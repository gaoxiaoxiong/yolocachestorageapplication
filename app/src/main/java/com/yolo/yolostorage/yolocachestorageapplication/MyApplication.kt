package com.yolo.yolostorage.yolocachestorageapplication

import android.app.Application

class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        //这是第二种配置方式，第一种是在清单文件里面配置
        //YoloCacheStorage.init(this)
    }
}