package com.yolo.yolostorage.yolocachestorageapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.yolo.cache.storage.YoloCacheStorage

class MainActivity : AppCompatActivity() {
    private val  TAG = "MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        YoloCacheStorage.put("sp_userName","张三")

        if(BuildConfig.DEBUG){
          Log.d(TAG, "获取到的数据=${YoloCacheStorage.getString("sp_userName","")}");
        }
    }
}